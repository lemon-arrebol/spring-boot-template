# 日志
## 统一日志
定义 logback 日志模版，统一日志输出格式以及固定输出信息

### 日志路径
JVM 启动参数 -Dlogging.app.path=/log 或者配置文件中如下配置设置日志路径
```
logging:
  app:
    request:
      log:
        enabled: true
```

### 日志级别
JVM 启动参数 -Dlogging.app.level=INFO -Dlogging.app.name.prefix=com.appPackage 或者配置文件中如下配置设置日志级别以及指定日志包
```
logging:
  app:
    level: INFO
    name:
      prefix: appPackage
```

### 日志写入Kafka
基于一下配置开启日志异步写入kafka中，可以自定义 Appender 将日志写入其它存储系统中。  
Kafka 主题名称规则: ${kafka_topic_prefix}_${applicationName}_${kafka_env}
```
logging:
  kafka:
    enabled: true
    broker: kafkaServerBroker
    topic:
        prefix: topic
    env: default
    acks: 0
    linger:
        ms: 1000
    max:
        block:
            ms: 0
    partition:
        num:
```

## 请求参/响应数日志
基于AOP以 @RequestMapping 以及衍生注解拦截请求，打印请求参数以及响应日志。如下配置开启请求参数/响应日志输出
```
logging:
  app:
    request:
      log:
        enabled: true
```