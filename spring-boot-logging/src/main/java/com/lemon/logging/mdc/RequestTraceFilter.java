//package com.lemon.web.mdc;
//
//import org.springframework.core.annotation.Order;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import java.io.IOException;
//
///**
// * @description 请求跟踪日志补充信息
// * @author lemon
// * @date 2020-07-03 12:13
// */
//@Order(1)
//@WebFilter(urlPatterns = {"/*"})
//public class RequestTraceFilter implements Filter {
//    /**
//     * @param
//     * @return
//     * @description
//     * @author Mcdull
//     * @date 2018/10/19
//     */
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//
//    }
//
//    /**
//     * @param
//     * @return
//     * @description
//     * @author Mcdull
//     * @date 2018/10/19
//     */
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        try {
//            RequestTrace.initRequestLog(servletRequest);
//            filterChain.doFilter(servletRequest, servletResponse);
//        } finally {
//            RequestTrace.resetRequestLog();
//        }
//    }
//
//    /**
//     * @param
//     * @return
//     * @description
//     * @author Mcdull
//     * @date 2018/10/19
//     */
//    @Override
//    public void destroy() {
//
//    }
//}
