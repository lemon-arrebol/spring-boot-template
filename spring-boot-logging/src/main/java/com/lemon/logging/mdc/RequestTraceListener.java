package com.lemon.logging.mdc;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

/**
 * @author lemon
 * @description 请求跟踪日志补充信息
 * @date 2020-07-03 12:13
 */
@WebListener
public class RequestTraceListener implements ServletRequestListener {
    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        RequestTrace.initRequestLog(servletRequestEvent.getServletRequest());
    }

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        RequestTrace.resetRequestLog();
    }
}