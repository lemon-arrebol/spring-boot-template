package com.lemon.logging.converter;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * @description
 * @author lemon
 * @date 2020-07-02 21:15
 */
public class ModuleConverter extends ClassicConverter {

    private static final int MAX_LENGTH = 20;

    @Override
    public String convert(ILoggingEvent event) {
//        if (event.getLoggerName().length() > MAX_LENGTH) {
//            return "";
//        } else {
//            return event.getLoggerName();
//        }

        return event.getLoggerName();
    }
}