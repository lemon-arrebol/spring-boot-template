package com.lemon.common.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author lemon
 * @description 非 Spring Bean 可以访问 Spring 管理的 Bean
 * @date 2020-07-05 08:11
 */
@Slf4j
@Component
public class SpringContextHelper implements ApplicationContextAware, BeanFactoryPostProcessor {

    /**
     * @description Spring应用上下文环境
     * @author lemon
     * @date 2019/2/22 12:04
     */
    private static ApplicationContext applicationContext;

    /**
     * @description Spring应用上下文环境
     * @author lemon
     * @date 2019/2/22 12:04
     */
    private static ConfigurableListableBeanFactory beanFactory;

    /**
     * @param
     * @return
     * @description 重写并初始化上下文
     * @author lemon
     * @date 2019/2/22 12:05
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextHelper.applicationContext = applicationContext;

        log.debug("ApplicationContext is {}", applicationContext);
    }

    /**
     * Modify the application context's internal bean factory after its standard
     * initialization. All bean definitions will have been loaded, but no beans
     * will have been instantiated yet. This allows for overriding or adding
     * properties even to eager-initializing beans.
     *
     * @param beanFactory the bean factory used by the application context
     * @throws BeansException in case of errors
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        SpringContextHelper.beanFactory = beanFactory;

        log.debug("ConfigurableListableBeanFactory is {}", beanFactory);
    }

    /**
     * @param
     * @return
     * @description 通过类获取
     * @author lemon
     * @date 2019/2/22 12:06
     */
    public static <T> T getBean(Class<T> clazz) throws BeansException {
        return SpringContextHelper.applicationContext.getBean(clazz);
    }

    /**
     * @param
     * @return
     * @description 通过名字获取
     * @author lemon
     * @date 2019/2/22 12:05
     */
    public static <T> T getBean(String name) throws BeansException {
        return (T) SpringContextHelper.applicationContext.getBean(name);
    }

    /**
     * @param
     * @return
     * @description 通过名字获取
     * @author lemon
     * @date 2019/2/22 12:05
     */
    public static <T> T getBean(String name, Class<T> clazz) throws BeansException {
        return SpringContextHelper.applicationContext.getBean(name, clazz);
    }

    /**
     * @param name
     * @return boolean
     * @description
     * @author lemon
     * @date 2020-07-10 09:50
     */
    public static boolean containsBean(String name) throws BeansException {
        return SpringContextHelper.applicationContext.containsBean(name);
    }
}