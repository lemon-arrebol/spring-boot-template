package com.lemon.mask.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lemon.mask.enumeration.SensitiveType;
import com.lemon.mask.serializer.JacksonDataMaskSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author lemon
 * @description 数据脱敏注解
 * @date 2020-07-01 19:00
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@JacksonAnnotationsInside
@JsonSerialize(using = JacksonDataMaskSerializer.class)
public @interface DataMask {
    /**
     * 脱敏数据类型
     */
    SensitiveType sensitiveType() default SensitiveType.DEFAULT;

    /**
     * 开始N个字符不脱敏
     */
    int prefixNoMaskSize() default Integer.MIN_VALUE;

    /**
     * 结尾N个字符不脱敏
     */
    int suffixNoMaskSize() default Integer.MIN_VALUE;

    /**
     * 脱敏字符串
     */
    String maskFlag() default "*";
}