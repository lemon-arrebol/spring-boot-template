package com.lemon.mask.annotation;

import com.lemon.mask.enumeration.SensitiveType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author lemon
 * @description 数据脱敏类型注解
 * @date 2020-07-01 19:00
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataMaskType {

    /**
     * 脱敏类型
     */
    SensitiveType sensitiveType();
}