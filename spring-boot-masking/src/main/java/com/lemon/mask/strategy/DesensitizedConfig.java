package com.lemon.mask.strategy;

import com.lemon.mask.enumeration.SensitiveType;
import lombok.Data;

/**
 * @author lemon
 * @version 1.0
 * @description: 脱敏配置
 * @date Create by lemon on 2020-07-03 16:38
 */
@Data
public class DesensitizedConfig {
    SensitiveType sensitiveType;

    /**
     * 前置不需要脱敏的长度
     */
    int prefixNoMaskSize;

    /**
     * 后置不需要脱敏的长度
     */
    int suffixNoMaskSize;

    /**
     * 脱敏字符串
     */
    String maskFlag;
}
