package com.lemon.mask.strategy;

import com.lemon.mask.annotation.DataMaskType;
import com.lemon.mask.enumeration.SensitiveType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lemon
 * @version 1.0
 * @description: [电子邮箱] 邮箱前缀仅显示第一个字母，前缀其他隐藏，用星号代替，@及后面的地址显示<例子:g**@163.com>
 * @date Create by lemon on 2020-07-03 14:53
 */
@Getter
@Component
@DataMaskType(sensitiveType = SensitiveType.EMAIL)
public class Email implements DataMaskStrategy {
    @Value("${mask.prefixNoMaskSize:1}")
    private int prefixNoMaskSize;

    @Value("${mask.suffixNoMaskSize:0}")
    private int suffixNoMaskSize;

    @Value("${mask.maskFlag:*}")
    private String maskFlag;

    /**
     * @param original
     * @return java.lang.String
     * @description [电子邮箱] 邮箱前缀仅显示第一个字母，前缀其他隐藏，用星号代替，@及后面的地址显示<例子:g**@163.com>
     * @author lemon
     * @date 2020-07-03 14:51
     */
    @Override
    public String mask(String original) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        int index = StringUtils.indexOf(original, "@");

        if (index <= 1) {
            return original;
        } else {
            String temp = mask(original.substring(0, index), prefixNoMaskSize, suffixNoMaskSize, maskFlag);

            if (StringUtils.isBlank(temp)) {
                return original;
            }

            return temp.concat(original.substring(index));
        }
    }
}
