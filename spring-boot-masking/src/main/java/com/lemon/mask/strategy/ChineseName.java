package com.lemon.mask.strategy;

import com.lemon.mask.annotation.DataMaskType;
import com.lemon.mask.enumeration.SensitiveType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lemon
 * @version 1.0
 * @description: [中文姓名] 只显示第一个汉字，其他隐藏为2个星号<例子：李**>
 * @date Create by lemon on 2020-07-03 14:53
 */
@Getter
@Component
@DataMaskType(sensitiveType = SensitiveType.CHINESE_NAME)
public class ChineseName implements DataMaskStrategy {
    @Value("${mask.prefixNoMaskSize:1}")
    private int prefixNoMaskSize;

    @Value("${mask.suffixNoMaskSize:1}")
    private int suffixNoMaskSize;

    @Value("${mask.maskFlag:*}")
    private String maskFlag;

    /**
     * @param original
     * @return java.lang.String
     * @description [中文姓名] 只显示第一个汉字，其他隐藏为2个星号<例子：李**>
     * @author lemon
     * @date 2020-07-03 14:51
     */
    @Override
    public String mask(String original) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        if (original.length() > 2) {
            return mask(original, prefixNoMaskSize, suffixNoMaskSize, maskFlag);
        } else {
            return mask(original, 1, 0);
        }
    }
}
