package com.lemon.mask.strategy;

import com.lemon.mask.annotation.DataMaskType;
import com.lemon.mask.enumeration.SensitiveType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lemon
 * @version 1.0
 * @description: 中文名
 * @date Create by lemon on 2020-07-03 14:53
 */
@Getter
@Component
@DataMaskType(sensitiveType = SensitiveType.PASSWORD)
public class Password implements DataMaskStrategy {
    @Value("${mask.prefixNoMaskSize:0}")
    private int prefixNoMaskSize;

    @Value("${mask.suffixNoMaskSize:0}")
    private int suffixNoMaskSize;

    @Value("${mask.maskFlag:******}")
    private String maskFlag;

    /**
     * @param original
     * @return java.lang.String
     * @description 原始数据脱敏处理
     * @author lemon
     * @date 2020-07-03 14:51
     */
    @Override
    public String mask(String original) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return maskFlag;
    }

    /**
     * @param original
     * @param prefixNoMaskSize
     * @param suffixNoMaskSize
     * @param maskFlag
     * @return java.lang.String
     * @description
     * @author lemon
     * @date 2020-07-06 12:27
     */
    @Override
    public String mask(String original, int prefixNoMaskSize, int suffixNoMaskSize, String maskFlag) {
        return this.mask(original);
    }
}
