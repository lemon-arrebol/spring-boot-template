package com.lemon.mask.strategy;

/**
 * @author lemon
 * @version 1.0
 * @description: 脱敏配置
 * @date Create by lemon on 2020-07-04 07:11
 */
public interface DataMaskConfig {
    /**
     * @param
     * @return java.lang.String
     * @description 前置不需要脱敏的长度
     * @author lemon
     * @date 2020-07-04 07:13
     */
    int getPrefixNoMaskSize();

    /**
     * @param
     * @return java.lang.String
     * @description 后置不需要脱敏的长度
     * @author lemon
     * @date 2020-07-04 07:13
     */
    int getSuffixNoMaskSize();

    /**
     * @param
     * @return java.lang.String
     * @description 脱敏字符串
     * @author lemon
     * @date 2020-07-04 07:13
     */
    String getMaskFlag();
}
