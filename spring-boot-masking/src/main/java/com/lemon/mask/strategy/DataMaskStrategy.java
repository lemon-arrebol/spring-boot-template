package com.lemon.mask.strategy;

import org.apache.commons.lang3.StringUtils;

/**
 * @author lemon
 * @version 1.0
 * @description: 数据脱敏策略
 * @date Create by lemon on 2020-07-03 14:26
 */
public interface DataMaskStrategy extends DataMaskConfig {
    /**
     * @param original
     * @return java.lang.String
     * @description 原始数据脱敏处理
     * @author lemon
     * @date 2020-07-03 14:51
     */
    String mask(String original);

    /**
     * @param desensitizedConfig
     * @param original
     * @return java.lang.String
     * @description 原始数据脱敏处理
     * @author lemon
     * @date 2020-07-04 07:08
     */
    default String mask(DesensitizedConfig desensitizedConfig, String original) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return this.mask(original, desensitizedConfig.getPrefixNoMaskSize(), desensitizedConfig.getSuffixNoMaskSize(),
                desensitizedConfig.getMaskFlag());
    }

    /**
     * @param original        原始数据
     * @param prefixNoMaskSize 开始N个字符不脱敏
     * @param suffixNoMaskSize 结尾N个字符不脱敏
     * @return java.lang.String
     * @description
     * @author lemon
     * @date 2020-07-03 15:25
     */
    default String mask(String original, int prefixNoMaskSize, int suffixNoMaskSize) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return this.mask(original, prefixNoMaskSize, suffixNoMaskSize, "*");
    }

    /**
     * @param original        原始数据
     * @param prefixNoMaskSize 开始N个字符不脱敏
     * @param suffixNoMaskSize 结尾N个字符不脱敏
     * @param maskFlag        脱敏字符串
     * @return java.lang.String
     * @description
     * @author lemon
     * @date 2020-07-03 14:54
     */
    default String mask(String original, int prefixNoMaskSize, int suffixNoMaskSize, String maskFlag) {
        // 默认脱敏
        if (prefixNoMaskSize == Integer.MIN_VALUE) {
            prefixNoMaskSize = this.getPrefixNoMaskSize();
        }

        if (suffixNoMaskSize == Integer.MIN_VALUE) {
            suffixNoMaskSize = this.getSuffixNoMaskSize();
        }

        // 空或者不需要脱敏
        if (StringUtils.isBlank(original) || (prefixNoMaskSize < 1 && suffixNoMaskSize < 1)) {
            return original;
        }

        if (StringUtils.isBlank(maskFlag)) {
            maskFlag = this.getMaskFlag();
        }

        String left = "";

        if (prefixNoMaskSize >= 1) {
            left = StringUtils.left(original, prefixNoMaskSize);
        }

        String right = "";

        if (suffixNoMaskSize >= 1) {
            right = StringUtils.right(original, suffixNoMaskSize);
        }

        original = StringUtils.rightPad(left, (original.length() - suffixNoMaskSize), maskFlag) + right;

        return original;
    }
}
