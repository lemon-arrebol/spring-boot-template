package com.lemon.mask.strategy;

import com.lemon.mask.annotation.DataMaskType;
import com.lemon.mask.enumeration.SensitiveType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lemon
 * @version 1.0
 * @description: [手机号码] 前三位，后四位，其他隐藏<例子:123******6789>
 * @date Create by lemon on 2020-07-03 14:53
 */
@Getter
@Component
@DataMaskType(sensitiveType = SensitiveType.MOBILE_PHONE)
public class MobilePhone implements DataMaskStrategy {
    @Value("${mask.prefixNoMaskSize:3}")
    private int prefixNoMaskSize;

    @Value("${mask.suffixNoMaskSize:4}")
    private int suffixNoMaskSize;

    @Value("${mask.maskFlag:*}")
    private String maskFlag;

    /**
     * @param original
     * @return java.lang.String
     * @description [手机号码] 前三位，后四位，其他隐藏<例子:123******6789>
     * @author lemon
     * @date 2020-07-03 14:51
     */
    @Override
    public String mask(String original) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return mask(original, prefixNoMaskSize, suffixNoMaskSize, maskFlag);
    }
}
