package com.lemon.mask.strategy;

import com.lemon.mask.annotation.DataMaskType;
import com.lemon.mask.enumeration.SensitiveType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lemon
 * @version 1.0
 * @description: [身份证号] 显示最后四位，其他隐藏。共计18位或者15位。<例子：*************5762>
 * @date Create by lemon on 2020-07-03 14:53
 */
@Getter
@Component
@DataMaskType(sensitiveType = SensitiveType.ID_CARD)
public class IdCard implements DataMaskStrategy {
    @Value("${mask.prefixNoMaskSize:0}")
    private int prefixNoMaskSize;

    @Value("${mask.suffixNoMaskSize:4}")
    private int suffixNoMaskSize;

    @Value("${mask.maskFlag:*}")
    private String maskFlag;

    /**
     * @param original
     * @return java.lang.String
     * @description [身份证号] 显示最后四位，其他隐藏。共计18位或者15位。<例子：*************5762>
     * @author lemon
     * @date 2020-07-03 14:51
     */
    @Override
    public String mask(String original) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return mask(original, prefixNoMaskSize, suffixNoMaskSize, maskFlag);
    }
}
