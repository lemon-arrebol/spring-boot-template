package com.lemon.mask.strategy;

import com.lemon.mask.annotation.DataMaskType;
import com.lemon.mask.enumeration.SensitiveType;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lemon
 * @version 1.0
 * @description: [公司开户银行联号] 公司开户银行联行号,显示前两位，其他用星号隐藏，每位1个星号<例子:12********>
 * @date Create by lemon on 2020-07-03 14:53
 */
@Getter
@Component
@DataMaskType(sensitiveType = SensitiveType.CNAPS_CODE)
public class CnapsCode implements DataMaskStrategy {
    @Value("${mask.prefixNoMaskSize:2}")
    private int prefixNoMaskSize;

    @Value("${mask.suffixNoMaskSize:0}")
    private int suffixNoMaskSize;

    @Value("${mask.maskFlag:*}")
    private String maskFlag;

    /**
     * @param original
     * @return java.lang.String
     * @description [公司开户银行联号] 公司开户银行联行号,显示前两位，其他用星号隐藏，每位1个星号<例子:12********>
     * @author lemon
     * @date 2020-07-03 14:51
     */
    @Override
    public String mask(String original) {
        if (StringUtils.isBlank(original)) {
            return original;
        }

        return mask(original, prefixNoMaskSize, suffixNoMaskSize, maskFlag);
    }
}
