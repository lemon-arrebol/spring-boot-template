package com.lemon.mask.builder;

import com.lemon.mask.annotation.DataMask;
import com.lemon.mask.strategy.DesensitizedConfig;
import com.lemon.mask.util.DesensitizedUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * @author lemon
 * @description 数据脱敏：重写toString方法实现数据脱敏
 * @date 2020-07-03 16:06
 */
public class DataMaskToStringBuilder extends ReflectionToStringBuilder {
    private boolean excludeNullValues = true;

    /**
     * @param object
     * @return
     * @description
     * @author lemon
     * @date 2020-07-03 17:29
     */
    public DataMaskToStringBuilder(Object object) {
        super(object);
    }

    /**
     * @param object
     * @param style
     * @return
     * @description
     * @author lemon
     * @date 2020-07-03 17:29
     */
    public DataMaskToStringBuilder(Object object, ToStringStyle style) {
        super(object, style);
    }

    /**
     * @param object
     * @param style
     * @return
     * @description
     * @author lemon
     * @date 2020-07-03 17:29
     */
    public DataMaskToStringBuilder(Object object, boolean excludeNullValues, ToStringStyle style) {
        super(object, style);
        this.excludeNullValues = excludeNullValues;
    }

    /**
     * @param clazz
     * @return void
     * @description
     * @author lemon
     * @date 2020-07-03 17:29
     */
    @Override
    protected void appendFieldsIn(Class clazz) {
        if (clazz.isArray()) {
            this.reflectionAppendArray(this.getObject());
            return;
        }

        Field[] fields = clazz.getDeclaredFields();
        AccessibleObject.setAccessible(fields, true);
        Arrays.stream(fields).forEach(this::appendFieldIn);
    }

    /**
     * @param field
     * @return void
     * @description
     * @author lemon
     * @date 2020-07-03 17:32
     */
    protected void appendFieldIn(Field field) {
        String fieldName = field.getName();

        if (!this.accept(field)) {
            return;
        }

        try {
            Object fieldValue = this.getValue(field);

            if (this.excludeNullValues && fieldValue == null) {
                return;
            }

            // 如果需要脱敏
            DataMask dataMask = field.getAnnotation(DataMask.class);

            if (dataMask == null || field.getType() != String.class) {
                this.append(fieldName, fieldValue);
            } else {
                this.appendMaskFieldIn(fieldName, (String) fieldValue, dataMask);
            }
        } catch (IllegalAccessException ex) {
            throw new InternalError("Unexpected IllegalAccessException: " + ex.getMessage());
        }
    }

    /**
     * @param fieldName
     * @param fieldValue
     * @param dataMask
     * @return void
     * @description
     * @author lemon
     * @date 2020-07-03 17:32
     */
    protected void appendMaskFieldIn(String fieldName, String fieldValue, DataMask dataMask) {
        DesensitizedConfig desensitizedConfig = DesensitizedUtils.getDesensitizedConfig(dataMask);
        this.append(fieldName, DesensitizedUtils.maskValue(fieldValue, desensitizedConfig));
    }
}
