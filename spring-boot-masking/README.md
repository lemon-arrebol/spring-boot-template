# 数据脱敏
## 日志数据脱敏
通常在输出对象日志数据时，会有些敏感数据(比如：密码、手机号、住址等)信息直接输出，存在 泄漏数据风险。
可以通过重写toString()方法，对敏感字段进行处理。  
一般有两种方案进行数据脱敏:  
(1) toString 方法中指定特定字段避免输出  
lombok @ToString annotation  
```
@ToString
public class Demo {
    private Integer id;
    private String name;
    @ToString.Exclude
    private String password;
}
```
commons-langs ToStringBuilder  
```
public class Demo {
    private Integer id;
    private String name;
    @ToStringExclude
    private String password;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
```
(2) 对特定字段加部分进行遮罩, 比如内容替换为'*'
基于 @DataMask 注解用于数据脱敏, 使用 DataMaskToStringBuilder 来重写 toString() 方法.
DataMaskToStringBuilder 底层使用的是 commons-langs ReflectionToStringBuilder.
指定脱敏范围
```
public class Demo {
    private Integer id;
    private String name;
    @DataMask(prefixNoMaskSize = 5, suffixNoMaskSize = 2, maskFlag = "#")
    private String password;
    
    @Override
    public String toString() {
        return new DataMaskToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
}
```
使用脱敏模板
```
public class Demo {
    private Integer id;
    private String name;
    @DataMask(sensitiveType = SensitiveType.PASSWORD)
    private String password;
    
    @Override
    public String toString() {
        return new DataMaskToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
}
```

##响应数据脱敏
接口返回的数据，如果存在敏感数据直接返回存在数据泄漏风险，可以在返回数据前将敏感数据脱敏。  
基于 @DataMask 注解用于数据脱敏，使用 JacksonDataMaskSerializer 在序列化响应数据为JSON时，将@DataMask 注解的属性进行脱敏。 

## @DataMask 
DataMask  注解上使用了  JsonSerialize 注解，指定了序列化时使用的序列化器材
```
@JacksonAnnotationsInside
@JsonSerialize(using = JacksonDataMaskSerializer.class)
public @interface DataMask {
    ......
}
```

