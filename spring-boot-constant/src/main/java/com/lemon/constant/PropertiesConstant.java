package com.lemon.constant;

/**
 * @author lemon
 * @version 1.0
 * @description: 配置前缀
 * @date Create by lemon on 2020-07-09 09:59
 */
public class PropertiesConstant {
    /**
     * 缓存配置是否开启
     */
    public static final String REDIS_CACHE_PROPERTIES_PREFIX = "spring.app.cache.redis";
}
