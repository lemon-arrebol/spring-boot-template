package com.lemon.constant;

/**
 * @author lemon
 * @version 1.0
 * @description: Config 配置开关
 * @date Create by lemon on 2020-07-09 09:59
 */
public class ConfigSwitchConstant {
    /**
     * 缓存配置是否开启
     */
    public static final String REDIS_CACHE_CONFIG_ENABLE = "spring.app.cache.enabled";

    /**
     * 缓存配置是否开启默认值，默认开启
     */
    public static final String REDIS_CACHE_CONFIG_ENABLE_DEFAULT = "true";

    /**
     * 统一响应结果是否开启
     */
    public static final String RESPONSE_BODY_ADVICE_ENABLE = "unified.result.enabled";

    /**
     * 统一响应结果否开启默认值，默认开启
     */
    public static final String RESPONSE_BODY_ADVICE_ENABLE_DEFAULT = "true";

    /**
     * 基于 @RequestMapping 注解输出请求入参、响应结果打印是否开启
     */
    public static final String LOGGER_ASPECT_ENABLE = "logging.app.request.log.enabled";

    /**
     * 基于 @RequestMapping 注解输出请求入参、响应结果打印是否开启默认值，默认开启
     */
    public static final String LOGGER_ASPECT_ENABLE_DEFAULT = "true";
}
