package com.lemon.exception.base;

/**
 * @author lemon
 * @description 业务异常
 * @date 2020-07-02 08:51
 */
public class BussinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static final String DEFAULT_MSG = "系统异常，请稍后重试";

    public BussinessException() {
        super(DEFAULT_MSG);
    }

    public BussinessException(String message) {
        super(message);
    }

    public BussinessException(Throwable cause) {
        super(cause);
    }

    public BussinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BussinessException(String message, Throwable cause,
                              boolean enableSuppression,
                              boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}