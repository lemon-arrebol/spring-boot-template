package org.springframework.boot.autoconfigure.data.redis;

import com.autoconfigure.redis.properties.RedisConnectionProperties;
import com.lemon.constant.ConfigSwitchConstant;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizers;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import redis.clients.jedis.Jedis;

import java.net.UnknownHostException;

/**
 * @author lemon
 * @version 1.0
 * @description: 创建 Jedis Redis 连接
 * @date Create by lemon on 2020-07-08 18:31
 */
@Configuration("jedisCacheManagerFactory")
@ConditionalOnClass({GenericObjectPool.class, JedisConnection.class, Jedis.class})
@ConditionalOnProperty(name = ConfigSwitchConstant.REDIS_CACHE_CONFIG_ENABLE, havingValue = ConfigSwitchConstant.REDIS_CACHE_CONFIG_ENABLE_DEFAULT, matchIfMissing = true)
public class JedisCacheManagerFactory extends AbstractCacheManagerFactory {
    private final ObjectProvider<RedisSentinelConfiguration> sentinelConfigurationProvider;

    private final ObjectProvider<RedisClusterConfiguration> clusterConfigurationProvider;

    private final ObjectProvider<JedisClientConfigurationBuilderCustomizer> builderCustomizersProvider;

    JedisCacheManagerFactory(CacheManagerCustomizers customizerInvoker,
                             ObjectProvider<RedisSentinelConfiguration> sentinelConfigurationProvider,
                             ObjectProvider<RedisClusterConfiguration> clusterConfigurationProvider,
                             ObjectProvider<JedisClientConfigurationBuilderCustomizer> builderCustomizersProvider) {
        super(customizerInvoker);
        this.sentinelConfigurationProvider = sentinelConfigurationProvider;
        this.clusterConfigurationProvider = clusterConfigurationProvider;
        this.builderCustomizersProvider = builderCustomizersProvider;
    }

    /**
     * @param properties
     * @return org.springframework.data.redis.connection.jedis.JedisConnectionFactory
     * @description
     * @author lemon
     * @date 2020-07-08 21:42
     */
    @Override
    public JedisConnectionFactory createConnectionFactory(RedisConnectionProperties properties) throws UnknownHostException {
        JedisConnectionConfiguration jedisConnectionConfiguration = new JedisConnectionConfiguration(properties,
                this.sentinelConfigurationProvider, this.clusterConfigurationProvider, this.builderCustomizersProvider);
        return jedisConnectionConfiguration.redisConnectionFactory();
    }

    /**
     * @param properties
     * @return org.springframework.cache.CacheManager
     * @description 根据配置创建缓存管理器
     * @author lemon
     * @date 2020-07-09 08:23
     */
    @Override
    public CacheManager createCacheManager(RedisConnectionProperties properties) throws UnknownHostException {
        JedisConnectionFactory redisConnectionFactory = this.createConnectionFactory(properties);
        return this.createCacheManager(redisConnectionFactory, properties);
    }
}
