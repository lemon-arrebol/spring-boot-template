package org.springframework.boot.autoconfigure.data.redis;

import com.autoconfigure.redis.properties.RedisConnectionProperties;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author lemon
 * @version 1.0
 * @description: 创建缓存管理器
 * @date Create by lemon on 2020-07-09 08:19
 */
public interface CacheManagerFactory {
    /**
     * @param properties
     * @return org.springframework.data.redis.connection.RedisConnectionFactory
     * @description 根据配置创建连接
     * @author lemon
     * @date 2020-07-09 16:58
     */
    RedisConnectionFactory createConnectionFactory(RedisConnectionProperties properties) throws Exception;

    /**
     * @param properties
     * @return org.springframework.cache.CacheManager
     * @description 根据配置创建缓存管理器
     * @author lemon
     * @date 2020-07-09 08:23
     */
    CacheManager createCacheManager(RedisConnectionProperties properties) throws Exception;

    /**
     * @param redisConnectionFactory
     * @param redisConnectionProperties
     * @return org.springframework.cache.CacheManager
     * @description 创建缓存管理器
     * @author lemon
     * @date 2020-07-09 15:51
     */
    CacheManager createCacheManager(RedisConnectionFactory redisConnectionFactory, RedisConnectionProperties redisConnectionProperties);



    /**
     * @param
     * @return org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair<java.lang.String>
     * @description
     * @author lemon
     * @date 2020-07-09 16:39
     */
    static RedisSerializationContext.SerializationPair<String> getSerializeKeysWith() {
        RedisSerializer<String> redisSerializer = new StringRedisSerializer();
        return RedisSerializationContext.SerializationPair.fromSerializer(redisSerializer);
    }

    /**
     * @param
     * @return org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair
     * @description
     * @author lemon
     * @date 2020-07-09 16:39
     */
    static RedisSerializationContext.SerializationPair getSerializeValuesWith() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);

        return RedisSerializationContext.SerializationPair.fromSerializer(jackson2JsonRedisSerializer);
    }
}
