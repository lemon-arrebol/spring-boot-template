package com.autoconfigure.redis.enumeration;

/**
 * @author lemon
 * @version 1.0
 * @description: TODO
 * @date Create by lemon on 2020-07-09 18:08
 */
public enum RedisClient {
    LETTUCE, JEDIS;
}
