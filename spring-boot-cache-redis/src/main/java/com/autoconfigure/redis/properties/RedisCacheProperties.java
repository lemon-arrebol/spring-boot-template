package com.autoconfigure.redis.properties;

import com.lemon.constant.PropertiesConstant;
import org.springframework.boot.autoconfigure.data.redis.CacheManagerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.redis.serializer.RedisSerializationContext;

/**
 * @author lemon
 * @version 1.0
 * @description: 多 Redis 缓存配置
 * @date Create by lemon on 2020-07-08 18:24
 */
@ConfigurationProperties(prefix = PropertiesConstant.REDIS_CACHE_PROPERTIES_PREFIX)
public class RedisCacheProperties extends CacheProperties<RedisConnectionProperties> {
    private RedisSerializationContext.SerializationPair<String> keySerializationPair = CacheManagerFactory.getSerializeKeysWith();

    private RedisSerializationContext.SerializationPair<Object> valueSerializationPair = CacheManagerFactory.getSerializeValuesWith();
}
