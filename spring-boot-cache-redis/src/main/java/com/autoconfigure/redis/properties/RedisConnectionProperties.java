package com.autoconfigure.redis.properties;

import com.google.common.collect.Lists;
import com.autoconfigure.redis.enumeration.RedisClient;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.data.redis.CacheManagerFactory;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.redis.cache.CacheKeyPrefix;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import java.time.Duration;
import java.util.List;

/**
 * @author lemon
 * @version 1.0
 * @description: Redis 连接配置
 * @date Create by lemon on 2020-07-08 13:14
 */
@Getter
@Setter
public class RedisConnectionProperties extends RedisProperties {
    /**
     * 缓存是否启用，默认启用
     */
    private Boolean enabled = true;

    /**
     * 缓存默认超时时间
     */
    private Duration ttl;

    /**
     * 是否缓存 NULL 值
     */
    private Boolean cacheNullValues;

    /**
     * 是否使用 前缀，默认使用
     */
    private Boolean usePrefix;

    private String keyDelimiter;

    /**
     * 缓存key空间：key 前缀, 避免 key 冲突
     */
    private String keyPrefix;

    /**
     * 缓存key空间：key 前缀, 避免 key 冲突
     */
    private CacheKeyPrefix cacheKeyPrefix;

    private RedisClient client = RedisClient.LETTUCE;

    private RedisSerializationContext.SerializationPair<String> keySerializationPair = CacheManagerFactory.getSerializeKeysWith();

    private RedisSerializationContext.SerializationPair<Object> valueSerializationPair = CacheManagerFactory.getSerializeValuesWith();

    private ConversionService conversionService;

    /**
     * 缓存名称
     */
    private List<String> cacheNames = Lists.newArrayList();
}
