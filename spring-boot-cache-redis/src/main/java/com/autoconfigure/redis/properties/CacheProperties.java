package com.autoconfigure.redis.properties;

import com.autoconfigure.redis.enumeration.RedisClient;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.redis.cache.CacheKeyPrefix;

import java.time.Duration;
import java.util.List;
import java.util.Map;

/**
 * @author lemon
 * @version 1.0
 * @description: 全局缓存配置
 * @date Create by lemon on 2020-07-08 12:53
 */
@Getter
@Setter
public class CacheProperties<T> {
    /**
     * 主缓存
     */
    private String primary = "default";

    /**
     * 应用使用缓存的空间，不同应用或同一应用不同子应用使用同一缓存存储时区分来
     */
    private String namespace;

    /**
     * 缓存默认超时时间
     */
    private Duration ttl;

    /**
     * 是否缓存 NULL 值
     */
    private Boolean cacheNullValues;

    /**
     * 是否使用 前缀，默认使用
     */
    private Boolean usePrefix;

    private String keyDelimiter;

    /**
     * 每个缓存连接 key 前缀
     */
    private String keyPrefix;

    /**
     * 缓存key空间：key 前缀, 避免 key 冲突
     */
    private CacheKeyPrefix cacheKeyPrefix;

    /**
     * Redis 客户端类型
     */
    private RedisClient client = RedisClient.LETTUCE;

    /**
     * 转换服务
     */
    private ConversionService conversionService;

    /**
     * 缓存名称
     */
    private List<String> cacheNames = Lists.newArrayList();

    /**
     * Redis 连接配置
     */
    private Map<String, T> connections = Maps.newHashMap();
}
