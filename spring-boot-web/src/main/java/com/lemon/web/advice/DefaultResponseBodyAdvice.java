package com.lemon.web.advice;

import com.lemon.constant.ConfigSwitchConstant;
import com.lemon.web.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.List;

/**
 * @author lemon
 * @version 1.0
 * @description: 统一响应结果
 * @date Create by lemon on 2020-07-06 17:14
 */
@Order(1)
@RestControllerAdvice
@ConditionalOnProperty(name = ConfigSwitchConstant.RESPONSE_BODY_ADVICE_ENABLE, havingValue = ConfigSwitchConstant.RESPONSE_BODY_ADVICE_ENABLE_DEFAULT, matchIfMissing = true)
public class DefaultResponseBodyAdvice implements ResponseBodyAdvice {
    @Value("#{'${beforeBodyWrite.ignore.urls:/error}'.toLowerCase().split(',')}")
    private List<String> ignoreUrls;

    /**
     * Whether this component supports the given controller method return type
     * * and the selected {@code HttpMessageConverter} type.
     *
     * @param returnType    the return type
     * @param converterType the selected converter type
     * @return {@code true} if {@link #beforeBodyWrite} should be invoked;
     * {@code false} otherwise
     */
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    /**
     * Invoked after an {@code HttpMessageConverter} is selected and just before
     * its write method is invoked.
     *
     * @param body                  the body to be written
     * @param returnType            the return type of the controller method
     * @param selectedContentType   the content type selected through content negotiation
     * @param selectedConverterType the converter type selected to write to the response
     * @param request               the current request
     * @param response              the current response
     * @return the body that was passed in or a modified (possibly new) instance
     */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        return ResponseUtil.beforeBodyWrite(body, returnType, selectedContentType, selectedConverterType, request, response, ignoreUrls);
    }
}
