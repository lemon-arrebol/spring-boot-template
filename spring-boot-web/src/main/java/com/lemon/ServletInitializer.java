package com.lemon;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author lemon
 * @description
 * @date 2020-07-02 10:56
 */
@Slf4j
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    @Override
    protected WebApplicationContext run(SpringApplication application) {
        WebApplicationContext webApplicationContext = super.run(application);
        Environment env = webApplicationContext.getEnvironment();

        log.info("\n----------------------------------------------------------\n\t" +
                        "服务端 '{}' 启动完成! \n\t" +
                        "环境: \t{} \n\t" +
                        "端口: \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"), env.getActiveProfiles(), env.getProperty("server.port"));

        return webApplicationContext;
    }
}
