## 通用功能
[配置加解密](spring-boot-config-deencrypt/README.md)  
[数据脱敏](spring-boot-masking/README.md)  
[统一异常/结果](spring-boot-web/README.md)  
[统一日志](spring-boot-logging/README.md)  

### backlog
[接口签名验证]()  
[接口参数加解密]()


# 问题收集
## pom.xml 配置
[Maven--pom.xml 配置详解](https://www.cnblogs.com/mingforyou/p/4494713.html)
[史上最全的maven的pom.xml文件详解](https://www.cnblogs.com/hafiz/p/5360195.html)

## spring boot 版本列表
[spring-cloud-dependencies 版本列表](https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-dependencies)  
[spring-boot-starter-parent 版本列表](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-parent)


[解决springboot配置@ControllerAdvice不能捕获NoHandlerFoundException问题](https://my.oschina.net/u/3049656/blog/1798583)  
[Spring Boot 自定义注解，AOP 切面统一打印出入参请求日志](https://www.cnblogs.com/Gent-Wang/p/11593316.html)

[logback kafkaAppender输出日志到kafka](https://www.cnblogs.com/lixyu/p/9629712.html)
[logback官方文档中文翻译第七章：Filters](https://www.jianshu.com/p/d3f5063c8e18)


[jackson-注解实现json序列化的时候自动进行数据脱敏](https://blog.csdn.net/liufei198613/article/details/79009015)

[spring boot 动态注入bean](https://www.cnblogs.com/eternityz/p/12241143.html)  
[Spring Bean 生命周期方法 InitializingBean、init-method、PostConstruct等](https://blog.csdn.net/hong10086/article/details/93505910)

[使用@AutoConfigureBefore调整配置顺序竟没生效？](https://www.cnblogs.com/yourbatman/p/13264743.html)

[一文搞懂springboot启动原理](https://www.jianshu.com/p/943650ab7dfd)

如果某些情况下不检查环境，可以在maven命令上加一个 -Denforcer.skip=true 来跳过enforcer插件执行。   

Podam Pojo填充随机值利器

jvm 启动参数如下 -Dlogging.app.path=/log logback中使用${logging.app.path:-./log}

若用到了logback的if标签的condition表达式，需要引入janino


