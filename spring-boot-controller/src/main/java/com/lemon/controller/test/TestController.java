package com.lemon.controller.test;

import com.google.common.collect.Lists;
import com.lemon.controller.test.dto.StdOrgReqDto;
import com.lemon.controller.test.entity.UcStdOrgRoleTree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {
    /**
     * \
     *
     * @return java.util.List<com.lemon.controller.test.entity.UcStdOrgRoleTree>
     * @description 加上 Error 参数不生效
     * @author lemon
     * @date 2020-07-02 19:42
     */
    @PostMapping("/org")
    public List<UcStdOrgRoleTree> getStandardOrganization(@RequestBody @Valid StdOrgReqDto stdOrdReqDto) {
        return Lists.newArrayList();
    }

    @GetMapping("/param")
    public String getRequestParam(HttpServletRequest httpServletRequest, @RequestParam(name = "stdOrdReqDto") @Valid @Max(66) Integer stdOrdReqDto) {
        log.debug("stdOrdReqDto is {}", stdOrdReqDto);
        return "RequestParam";
    }

    @GetMapping("/pathVariable")
    public String getPathVariable(@PathVariable(name = "stdOrdReqDto") @Valid Integer stdOrdReqDto) {
        return "RequestParam";
    }
}