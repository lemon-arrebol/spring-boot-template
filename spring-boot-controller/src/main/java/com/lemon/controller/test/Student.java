package com.lemon.controller.test;

import com.lemon.mask.annotation.DataMask;
import com.lemon.mask.builder.DataMaskToStringBuilder;
import com.lemon.mask.enumeration.SensitiveType;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringStyle;

@Data
public class Student {
    @DataMask(sensitiveType = SensitiveType.CHINESE_NAME)
    private String chinaName;
    @DataMask(sensitiveType = SensitiveType.PASSWORD)
    private String moble_phone;
    @DataMask(prefixNoMaskSize = 3, suffixNoMaskSize = 2, maskFlag = "@")
    private String carNumber;

    @Override
    public String toString() {
        return new DataMaskToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
}