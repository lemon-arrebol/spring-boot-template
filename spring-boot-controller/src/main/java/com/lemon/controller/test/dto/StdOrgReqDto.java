package com.lemon.controller.test.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author lemon
 * @version 1.0
 * @description: 标准组织请求参数
 * @date Create by lemon on 2020-06-12 10:58
 */
@Getter
@Setter
@ToString(callSuper = true)
public class StdOrgReqDto implements Serializable {
    /**
     * 父组织ID
     */
    private String parentId;

    /**
     * 通用角色ID
     */
    private String commonStdRoleId;

    /**
     * 当前登录用户
     */
    @NotBlank(message = "参数loginName不允许为空")
    private String loginName;

    /**
     * 功能 1 标准组织类别，2 标准角色选择
     */
    @NotNull(message = "功能类型不允许为空")
    private Integer funType;

    /**
     * 限定范围 -100 无限定范围，1 限定标准组织范围
     */
    @NotNull(message = "限定范围不允许为空")
    private Integer limitScope;

    /**
     * 0/默认 懒加载，1 全量
     */
    @Max(66)
    private Integer all;
}
