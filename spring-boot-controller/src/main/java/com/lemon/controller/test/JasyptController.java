package com.lemon.controller.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("jasypt")
@Slf4j
public class JasyptController {
    @Value("${jasyptTest:}")
    private String jasypt;

    @GetMapping(value = {"index"})
    public String index() {
        return jasypt;
    }
}