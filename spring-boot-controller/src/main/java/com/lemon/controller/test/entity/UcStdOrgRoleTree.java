package com.lemon.controller.test.entity;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author lemon
 * @version 1.0
 * @description: 标准组织树
 * @date Create by lemon on 2019-07-18 16:24
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
public class UcStdOrgRoleTree implements Serializable {
    /**
     * 标准角色/标准组织ID
     */
    private String id;

    /**
     * 标准角色/标准组织名称
     */
    private String name;

    /**
     * 标准角色/标准组织名称全路径
     */
    private String namePath;

    /**
     * 树元素类型：1 组织，2角色
     */
    private Integer treeType;

    private String iconCls;

    private Integer ownorids;

    /**
     * 0 close 关闭 1 open 打开
     */
    private Integer state;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 0 岗位标准角色 1业务标准角色
     * UC前端逻辑设置为1
     */
    private Integer roleType;

    /**
     * 角色等级
     */
    private Integer roleLevel;

    /**
     * 组织编码
     */
    private String orgCode;

    /**
     * 组织名称 zzname
     */
    private String orgName;

    /**
     * 部门 bmname
     */
    private String deptName;

    /**
     * 中心 zxname
     */
    private String centerName;

    /**
     * 组 zname
     */
    private String groupName;

    /**
     * 层级
     */
    private Integer level;

    private List<UcStdOrgRoleTree> sonList = Lists.newArrayList();
}
