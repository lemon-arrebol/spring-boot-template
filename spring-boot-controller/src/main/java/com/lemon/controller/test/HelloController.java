package com.lemon.controller.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("hello")
@Slf4j
public class HelloController {
    @Cacheable(cacheNames = "aa")
    @PostMapping(value = {"index"})
    public Student ceshi(@RequestBody Student student) {
        log.info("这个月的学生 student:[{}]", student);

        Student result = new Student();
        result.setChinaName("sadsad在线啊结合实际大");
        result.setMoble_phone("1282478249834");
        result.setCarNumber("sdasdasdasdasdas");
        return result;
    }
}