
### 配置文件敏感信息密文解密配置
```
jasypt:
  encryptor:
    property:
      prefix: '{ENC}'
      suffix: ''
    password: qwertyuiop
    algorithm: PBEWithMD5AndDES
```

### 配置密码

#### 配置在配置文件文件中（这种方式不安全）
```
jasypt:
  encryptor:
    password: qwertyuiop
```

#### 在JVM启动参数中设置
```
-Djasypt.encryptor.password=qwertyuiop
```